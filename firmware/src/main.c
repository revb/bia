#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "system/common/sys_module.h"

#include <xc.h>
/* 
 * Main program entry point
 * 
 * For those who care this was generated by MPLAB. All BIA-specific
 * code is in bia.c
 */
int main ( void )
{
    /* Initialize all MPLAB Harmony modules, including application(s). */
    SYS_Initialize ( NULL );


    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );

    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}
