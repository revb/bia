#include "iohal.h"
#include "cfg.h"
#include "system_definitions.h"

void IO_LED_Set(uint32_t leds)
{
    leds ^= 0xFFFFFFFF;
    IO_LED_1 = (leds) & 0x01;
    IO_LED_2 = (leds >> 1) & 0x01;
    IO_LED_3 = (leds >> 2) & 0x01;
    IO_LED_4 = (leds >> 3) & 0x01;
    IO_LED_5 = (leds >> 4) & 0x01;
    IO_LED_6 = (leds >> 5) & 0x01;
    IO_LED_7 = (leds >> 6) & 0x01;
    IO_LED_8 = (leds >> 7) & 0x01;
    IO_LED_9 = (leds >> 8) & 0x01;
    IO_LED_10 = (leds >> 9) & 0x01;
    IO_LED_11 = (leds >> 10) & 0x01;
    IO_LED_12 = (leds >> 11) & 0x01;
    IO_LED_13 = (leds >> 12) & 0x01;
    IO_LED_14 = (leds >> 13) & 0x01;
    IO_LED_15 = (leds >> 14) & 0x01;
    IO_LED_16 = (leds >> 15) & 0x01;
    IO_LED_17 = (leds >> 16) & 0x01;
    IO_LED_18 = (leds >> 17) & 0x01;
}

uint32_t IO_BTN_Get()
{
    uint32_t ret =  IO_BTN_1 |
                    IO_BTN_2 << 1 |
                    IO_BTN_3 << 2 |
                    IO_BTN_4 << 3 |
                    IO_BTN_5 << 4 |
                    IO_BTN_6 << 5 |
                    IO_BTN_7 << 6 |
                    IO_BTN_8 << 7 |
                    IO_BTN_9 << 8 |
                    IO_BTN_10 << 9 |
                    IO_BTN_11 << 10 |
                    IO_BTN_12 << 11 |
                    IO_BTN_13 << 12 |
                    IO_BTN_14 << 13 |
                    IO_BTN_15 << 14 |
                    IO_BTN_16 << 15 |
                    IO_BTN_17 << 16 |
                    IO_BTN_18 << 17;
    ret ^= 0xFFFFFFFF;
    
    //mask out any disabled buttons
    uint8_t i;
    for (i=0;i<18;i++)
    {
        if (!btn_config_list[i].enabled)
            ret &= ~(0x1 << i);
    }
    
    return ret;
}

uint8_t IO_BTN_GetSingle(uint8_t btn)
{
    switch (btn)
    {
        case 1:
            return IO_BTN_1;
            break;
        case 2:
            return IO_BTN_2;
            break;
        case 3:
            return IO_BTN_3;
            break;
        case 4:
            return IO_BTN_4;
            break;
        case 5:
            return IO_BTN_5;
            break;
        case 6:
            return IO_BTN_6;
            break;
        case 7:
            return IO_BTN_7;
            break;
        case 8:
            return IO_BTN_8;
            break;
        case 9:
            return IO_BTN_9;
            break;
        case 10:
            return IO_BTN_10;
            break;
        case 11:
            return IO_BTN_11;
            break;
        case 12:
            return IO_BTN_12;
            break;
        case 13:
            return IO_BTN_13;
            break;
        case 14:
            return IO_BTN_14;
            break;
        case 15:
            return IO_BTN_15;
            break;
        case 16:
            return IO_BTN_16;
            break;
        case 17:
            return IO_BTN_17;
            break;
        case 18:
            return IO_BTN_18;
            break;            
        default:
            return 0;
    }
}
