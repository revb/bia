#ifndef IOHAL_H
#define	IOHAL_H

#include <stdint.h>
#include "iomap.h"

/*
 * Set the state of the LEDs. Each LED is represented by a bit.
 * 0 = off, 1 = on
 * 
 * Bits 0-17 correspond to LEDs 0-17
 */
void IO_LED_Set(uint32_t leds);


/*
 * Get the state of all the buttons. Each button is represented
 * by a bit.
 * 0 = off, 1 = on
 * 
 * Any button that is disabled will be set to 0.
 * 
 * Bits 0-17 correspond to buttons 0-17
 */
uint32_t IO_BTN_Get();

/*
 * Get the state of a single button.
 * 
 * If the button number is invalid (< 1 or > 18) then this function
 * will return 0. This function will ignore any disabled button and 
 * always return the physical state of the button's corresponding pin
 * 
 * Parameters:
 *      btn         The button to get
 */
uint8_t IO_BTN_GetSingle(uint8_t btn);


//unimplemented
uint8_t IO_ENC_GetDelta();


#endif	/* IOHAL_H */

