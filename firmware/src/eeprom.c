#include "eeprom.h"
#include "system_definitions.h"

#define HANDLE_WAIT(h) while (!(DRV_SPI_BUFFER_EVENT_COMPLETE & DRV_SPI_BufferStatus(h))) {}
//chip select macros
#define ACTIVATE() SYS_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_4, 0)
#define STANDBY() SYS_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_4, 1)

void EEPROM_Init()
{
    STANDBY();
}

void EEPROM_Write(uint8_t addr, uint8_t data)
{
    DRV_SPI_BUFFER_HANDLE whandle,rhandle;
    DRV_HANDLE handle;
    uint8_t sr;
    
    //commands to send to the EEPROM chip to write data
    uint8_t TXbuffer[] = {
        EEPROM_RDSR,
        EEPROM_WREN, //this one is sent first, separately
        EEPROM_WRITE,
        addr,
        data
    };
    
    //open the SPI driver
    handle = DRV_SPI_Open(DRV_SPI_INDEX_0, DRV_IO_INTENT_READWRITE);
    
    //query the EEPROM status register & wait for pending write cycles to complete
    sr = 1;
    while (sr & 0x01)
    {
        ACTIVATE();
        whandle = DRV_SPI_BufferAddWrite(handle, &TXbuffer[0], 2, 0, 0);
        HANDLE_WAIT(whandle);
        rhandle = DRV_SPI_BufferAddRead(handle, &sr, 1, 0, 0);
        HANDLE_WAIT(rhandle);
        STANDBY();
    }
    
    //first, enable the write latch
    ACTIVATE();
    whandle = DRV_SPI_BufferAddWrite(handle, &TXbuffer[1], 1, 0, 0);
    HANDLE_WAIT(whandle);
    STANDBY();
    
    //next, send the write command, address, and data
    ACTIVATE();
    whandle = DRV_SPI_BufferAddWrite(handle, &TXbuffer[2], 3, 0, 0);
    HANDLE_WAIT(whandle);
    STANDBY();
    
    DRV_SPI_Close(handle); //close the driver handle
}

void EEPROM_Read(uint8_t addr, uint8_t* data)
{
    DRV_SPI_BUFFER_HANDLE whandle, rhandle;
    DRV_HANDLE handle;
    uint8_t sr;
    
    //commands to send to the EEPROM to start reading
    uint8_t TXbuffer[] = {
        EEPROM_RDSR,
        EEPROM_READ,
        addr,
    };
    
    //open the SPI driver
    handle = DRV_SPI_Open(DRV_SPI_INDEX_0, DRV_IO_INTENT_READWRITE);
    
    //query the EEPROM status register & wait for pending write cycles to complete
    sr = 1;
    while (sr & 0x01)
    {
        ACTIVATE();
        whandle = DRV_SPI_BufferAddWrite(handle, &TXbuffer[0], 2, 0, 0);
        HANDLE_WAIT(whandle);
        rhandle = DRV_SPI_BufferAddRead(handle, &sr, 1, 0, 0);
        HANDLE_WAIT(rhandle);
        STANDBY();
    }
    
    ACTIVATE();
    //send the read command and address
    whandle = DRV_SPI_BufferAddWrite(handle, &TXbuffer[1], 2, 0, 0);
    HANDLE_WAIT(whandle);
    //send dummy data while receiving
    rhandle = DRV_SPI_BufferAddRead(handle, data, 1, 0, 0);
    HANDLE_WAIT(rhandle);
    STANDBY();
    
    DRV_SPI_Close(handle); //close the driver handle
}