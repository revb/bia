#ifndef USBMGR_H
#define	USBMGR_H

#include <stdint.h>
#include <stdbool.h>
#include "system_definitions.h"
    
/* Data that makes up an output report (sent FROM the PC) */
typedef struct {
    uint8_t report_id;
    uint32_t leds : 24; //we have to specify a bit size here since the LED data is only 24 bits
    uint32_t cfg_cmd;
} USB_OutputReport_t;

/* Data that makes up an input report (sent TO the PC) */
typedef struct {
    uint8_t report_id;
    uint8_t x;
    uint8_t y;
    uint32_t buttons : 24; //we have to specify a bit size here since the button data is only 24 bits
    uint32_t cfg_cmd;
} __attribute__((packed)) USB_InputReport_t;


/* Initializes the USB manager */
bool USBMGR_Initialize();
/* Callback from the USB driver when a HID event occurrs */
void USBMGR_HIDEventHandler(USB_DEVICE_HID_INDEX instanceIndex, USB_DEVICE_HID_EVENT event, void* pData, uintptr_t context);
/* Callback from the USB driver when a device evenet occurrs */
void USBMGR_DeviceEventHandler(USB_DEVICE_EVENT event, void* eventData, uintptr_t context);

#endif	/* USBMGR_H */

