/* 
 * File:   eeprom.h
 * Author: justin
 *
 * Created on February 3, 2016, 9:39 AM
 */

#ifndef EEPROM_H
#define	EEPROM_H

#include <stdint.h>

//command definitions
#define EEPROM_WREN 0x06
#define EEPROM_WRITE 0x02
#define EEPROM_READ 0x03
#define EEPROM_RDSR 0x05
#define EEPROM_DUMMY 0x00

/* External memory driver interface */

void EEPROM_Init();

/*
 * Write to the EEPROM
 * 
 * Parameters:
 *      addr    8-bit memory address
 *      data    8-bit data
 * Returns: none
 */
void EEPROM_Write(uint8_t addr, uint8_t data);

/*
 * Read data from the EEPROM
 * 
 * Parameters:
 *      addr    8-bit address
 *      data    Pointer to buffer to receive data
 * Returns:
 *      8-bit data
 */
void EEPROM_Read(uint8_t addr, uint8_t* data);

#endif	/* EEPROM_H */

