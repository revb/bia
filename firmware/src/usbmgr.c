#include "usbmgr.h"
#include "app.h"


/* Data is stored in app.c, just reference it here*/
extern APP_DATA appData;

bool USBMGR_Initialize()
{
    appData.deviceHandle = USB_DEVICE_Open( USB_DEVICE_INDEX_0,DRV_IO_INTENT_READWRITE );
    
    if (appData.deviceHandle == USB_DEVICE_HANDLE_INVALID)
        return false;

    //now that we have a driver handle, register the device event handler
    USB_DEVICE_EventHandlerSet(appData.deviceHandle, USBMGR_DeviceEventHandler, 0);
    
    return true;
}


void USBMGR_HIDEventHandler(USB_DEVICE_HID_INDEX instanceIndex, USB_DEVICE_HID_EVENT event, void* pData, uintptr_t context)
{
    USB_DEVICE_HID_EVENT_DATA_REPORT_SENT * reportSent;
    USB_DEVICE_HID_EVENT_DATA_REPORT_RECEIVED * reportReceived;
    
    switch(event)
    {
        case USB_DEVICE_HID_EVENT_REPORT_SENT:
            reportSent = (USB_DEVICE_HID_EVENT_DATA_REPORT_SENT *) pData;
            if(reportSent->handle == appData.sendTransferHandle )
            {
                // Transfer progressed.
                appData.isReportSentComplete = true;
            }
            
            break;
        case USB_DEVICE_HID_EVENT_REPORT_RECEIVED:
            USB_DEVICE_ControlStatus(appData.deviceHandle, USB_DEVICE_CONTROL_STATUS_OK);
            reportReceived = (USB_DEVICE_HID_EVENT_DATA_REPORT_RECEIVED *) pData;
            if(reportReceived->handle == appData.receiveTransferHandle )
            {
                // Transfer progressed.
                appData.isReportReady = true;
            }
            
            break;
         case USB_DEVICE_HID_EVENT_SET_IDLE:

            /* For now we just accept this request as is. We acknowledge
             * this request using the USB_DEVICE_HID_ControlStatus()
             * function with a USB_DEVICE_CONTROL_STATUS_OK flag */

            USB_DEVICE_ControlStatus(appData.deviceHandle, USB_DEVICE_CONTROL_STATUS_OK);

            /* save Idle rate recieved from Host */
            appData.idleRate = ((USB_DEVICE_HID_EVENT_DATA_SET_IDLE*)pData)->duration;
            break;
        case USB_DEVICE_HID_EVENT_GET_IDLE:

            /* Host is requesting for Idle rate. Now send the Idle rate */
            USB_DEVICE_ControlSend(appData.deviceHandle, &(appData.idleRate),1);
            /* On successfully reciveing Idle rate, the Host would acknowledge back with a
               Zero Length packet. The HID function driver returns an event
               USB_DEVICE_HID_EVENT_CONTROL_TRANSFER_DATA_SENT to the application upon
               receiving this Zero Length packet from Host.
               USB_DEVICE_HID_EVENT_CONTROL_TRANSFER_DATA_SENT event indicates this control transfer
               event is complete */

            break;
        default:
            break;
    }
    
    return USB_DEVICE_HID_EVENT_RESPONSE_NONE;
}

void USBMGR_DeviceEventHandler(USB_DEVICE_EVENT event, void* eventData, uintptr_t context)
{
    //USB_DEVICE_EVENT_DATA_CONFIGURED * configurationValue;
    
    switch(event)
    {
        case USB_DEVICE_EVENT_RESET:
        case USB_DEVICE_EVENT_DECONFIGURED:
            appData.isConfigured = 0;
            appData.state = APP_STATE_WAIT_FOR_CONFIG;
            break;
        case USB_DEVICE_EVENT_CONFIGURED:
            appData.isConfigured = 1;
            
            appData.configurationValue = ((USB_DEVICE_EVENT_DATA_CONFIGURED*)eventData)->configurationValue;
            
            //register the HID event handler.
            USB_DEVICE_HID_EventHandlerSet(USB_DEVICE_HID_INDEX_0, USBMGR_HIDEventHandler, (uintptr_t)&appData);
            break;
        case USB_DEVICE_EVENT_SUSPENDED:
            break;
        case USB_DEVICE_EVENT_POWER_DETECTED:
            //attach the device when connected to a host
            USB_DEVICE_Attach (appData.deviceHandle);
            break;
        case USB_DEVICE_EVENT_POWER_REMOVED:
            //detach the device when disconnected from a host
            USB_DEVICE_Detach (appData.deviceHandle);
            break;
        case USB_DEVICE_EVENT_RESUMED:
        case USB_DEVICE_EVENT_ERROR:
        default:
            break;
    }
}