#include "app.h"
#include "usbmgr.h"
#include "iohal.h"
#include "cfg.h"
#include "eeprom.h"
#include "quadencoder.h"

#include <stdint.h>
#include <stdbool.h>

/*
 * Why in the everloving shit does the recieve buffer's size need
 * to be the same as the endpoint's size even if the data only uses
 * 4 bytes. There is no need for this and worked fine in the MLA.
 * Stupid API developers are stupid.
 */
uint8_t  receiveDataBuffer[64] __attribute__((coherent, aligned(4)));
USB_OutputReport_t* usb_output_report;

//the transmit data buffer (buttons, encoders, etc)
uint8_t  transmitDataBuffer[sizeof(USB_InputReport_t)] __attribute__((coherent, aligned(4)));
USB_InputReport_t* usb_input_report;

//the application data
APP_DATA appData;

void APP_Initialize ( void )
{
    //state machine will start at the initialize state
    appData.state = APP_STATE_INIT;
    
    //initialize USB handles
    appData.deviceHandle = USB_DEVICE_HANDLE_INVALID;
    
    appData.isConfigured = false;
    appData.isReportReady = false;
    appData.isReportSentComplete = true;
    
    appData.configMode = false;
    
    appData.receiveTransferHandle = USB_DEVICE_HID_TRANSFER_HANDLE_INVALID;
    appData.sendTransferHandle = USB_DEVICE_HID_TRANSFER_HANDLE_INVALID;
    
    usb_output_report = (USB_OutputReport_t*)&receiveDataBuffer[0];
    usb_input_report = (USB_InputReport_t*)&transmitDataBuffer[0];
    
    EEPROM_Init();
    CFG_Initialize();
    QE_Initialize();
}

void APP_Tasks ( void )
{
    /* Main application state machine */
    switch ( appData.state )
    {
        case APP_STATE_INIT:
            //note: the USB driver can take a few cycles to initialize before we can
            //get a handle to it. Therefore we will stay in this state until we are able to
            //successfully get a handle
            if (USBMGR_Initialize())
                appData.state = APP_STATE_WAIT_FOR_CONFIG;
            break;
        case APP_STATE_WAIT_FOR_CONFIG:
            //wait for the USB driver to be configured by the host
            if (appData.isConfigured)
            {
                appData.isReportReady = false;
                appData.isReportSentComplete = true;
                appData.state = APP_STATE_RUNNING;
                
                //place a new read request to start message pump
                USB_DEVICE_HID_ReportReceive(USB_DEVICE_HID_INDEX_0, &appData.receiveTransferHandle, usb_output_report, sizeof(receiveDataBuffer));
            }
            break;
        case APP_STATE_RUNNING:
        {
            //in this state we will handle all of the application's main logic
            
            //TODO: implement literally everything
            //lol I hate USB so much HAHAHAHAHAHAHAHAHAhelpmeHAHAHAHAHAHA
            
            if (!appData.isConfigured)
            {
                appData.state = APP_STATE_WAIT_FOR_CONFIG;
                break;
            }
            
            //process any encoder changes
            QE_Update();
            
            //if a report has been received, we can read it now and update the LEDs
            if (appData.isReportReady)
            {
                appData.isReportReady = false;
                
                //get the received report
                USB_DEVICE_HID_ReportReceive(USB_DEVICE_HID_INDEX_0, &appData.receiveTransferHandle, usb_output_report, 64);
                
                //first we check if config commands were sent - if it was don't update anything else
                if (!CFG_ProcessCmd(usb_output_report->cfg_cmd))
                {
                    //update the LEDs
                    IO_LED_Set(usb_output_report->leds);
                }
            }
            
            //if a report has been sent successfully, we can now send another one
            if (!appData.configMode && appData.isReportSentComplete)
            {
                appData.isReportSentComplete = false;
                
                //report ID is always 1 for the game pad data
                usb_input_report->report_id = 1;
                //no config commands are sent here
                usb_input_report->cfg_cmd = 0;
                //set the buttons
                usb_input_report->buttons = IO_BTN_Get();
                usb_input_report->x = QE_GetPosition(0);
                usb_input_report->y = QE_GetPosition(1);
                //send the input report
                USB_DEVICE_HID_ReportSend(USB_DEVICE_HID_INDEX_0, &appData.sendTransferHandle, usb_input_report, sizeof(USB_InputReport_t));
            }
            
            break;
        }
        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

