#ifndef CMDS_H
#define	CMDS_H

#define CMD_NOP 0x00

/* Commands that can be received from the PC */

#define CMD_ENTER_CFG 0x01
#define CMD_EXIT_CFG 0x02

/*
 * CMD_QUAD
 * 
 * Configures 2 buttons as quadrature encoder A/B inuts
 * 
 * The following buttons may be used as encoder inputs:
 *      4-9, 12-18
 * 
 * If bytes 2 and 3 are both 0xFF then the buttons attached to the encoder
 * will be turned back into normal buttons.
 * 
 * Parameters:
 *      Byte 1: Encoder to map to (1 or 2)
 *      Byte 2: Button to map as A input
 *      Byte 3: Button to map as B input
 * 
 * Returns:
 *      ACK_ERR: Failed to set configuration
 *      ACK_OK: Success
 */
#define CMD_QUAD  0x10

/*
 * CMD_QUAD_PPR
 * 
 * Set the PPR or an encoder
 * 
 * Parameters:
 *      Byte 1: Encoder #
 *      Byte 2: PPR high byte
 *      Byte 3: PPR low byte
 * 
 * Returns:
 *      ACK_ERR: Failed to set configuration
 *      ACK_OK: Success
 */
#define CMD_QUAD_PPR 0x11

/*
 * CMD_QUAD_SENS
 * 
 * Set the sensitivity. 
 * 
 * Range: 0.1 -> 2.0
 * 
 * Parameters:
 *      Byte 1: Encoder #
 *      Byte 2: sensitivity (cast to float)
 * 
 * Returns:
 *      ACK_ERR: Failed to set configuration
 *      ACK_OK: Success
 */
#define CMD_QUAD_SENS 0x12

/*
 * CMD_SET_LED
 * 
 * Set an LED as dimmable or not
 * 
 * The following LEDs may be used as dimmable outputs:
 *      4-7
 * 
 * Parameters:
 *      Byte 1: LED # to map
 *      Byte 2: 1 if dimmable, 0 if not
 *      Byte 3: Not used
 * 
 * Returns:
 *      ACK_ERR: Failed to set configuration
 *      ACK_OK: Success
 */
#define CMD_SET_LED 0x20


/*
 * CMD_REQ_BTN
 * 
 * PC requested the configuration of an button
 * 
 * Parameters:
 *      Byte 1: BTN # to get
 *      Byte 2: Not used
 *      Byte 3: Not used
 * 
 * Returns:
 *      ACK_BTN
 */
#define CMD_REQ_BTN 0x40

/*
 * CMD_REQ_LED
 * 
 * PC requested the configuration of an LED
 * 
 * Parameters:
 *      Byte 1: LED # to get
 *      Byte 2: Not used
 *      Byte 3: Not used
 * 
 * Returns:
 *      ACK_LED
 */
#define CMD_REQ_LED 0x41

/*
 * CMD_REQ_QUAD
 * 
 * PC requested the configuration of an encoder
 * 
 * Parameters:
 *      Byte 1: Encoder # to get
 *      Byte 2: Not used
 *      Byte 3: Not used
 * 
 * Returns:
 *      ACK_QUAD
 */
#define CMD_REQ_QUAD 0x42


/* Commands that can be sent to the PC*/
#define ACK_OK 0x10
#define ACK_ERR 0x11

/*
 * ACK_BTN
 * 
 * Returned when the PC requests the state of a button
 * 
 * Returns:
 *      Byte 1: Encoder # (or 0 if normal button)
 *      Byte 2: 0 if A channel, 1 if B channel
 *      Byte 3: Not used
 */
#define ACK_BTN 0x20

/*
 * ACK_LED
 * 
 * Returned when the PC requests the configuration of an LED
 * 
 * Returns:
 *      Byte 1: 1 if dimmable
 */
#define ACK_LED 0x30

/*
 * ACK_NAME
 * 
 * Returns:
 *      3 characters in name
 */
#define ACK_NAME 0x40

/*
 * ACK_QUAD
 * 
 * Returned when the PC requests the configuration of an encoder
 * 
 * Returns:
 *      Byte 1: PPR high byte
 *      Byte 2: PPR low byte
 *      Byte 3: sensitivity
 */
#define ACK_QUAD 0x50


#endif	/* CMDS_H */

