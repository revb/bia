#ifndef CFG_H
#define	CFG_H

#include "system_definitions.h"

#define NAME_MAX_LENGTH 20

/* Variables that store the configuration data */
typedef struct {
    uint8_t enabled;
    uint8_t A,B;
    float sensitivity;
    uint8_t ppr;
} EncoderConfig;
typedef struct {
    uint8_t enabled;
    uint8_t encoder;
} ButtonConfig;
typedef struct {
    uint8_t dimmable;
} LEDConfig;
EncoderConfig enc_conf_list[2];
ButtonConfig btn_config_list[18];
LEDConfig led_config_list[18];
char conf_device_name[NAME_MAX_LENGTH];

void CFG_Initialize();

int CFG_ProcessCmd(uint32_t cmd);

#endif	/* CFG_H */

