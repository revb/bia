#ifndef QUADENCODER_H
#define	QUADENCODER_H

/*
 * Quadrature encoder driver.
 * 
 * This driver is interrupt driven - the position of the encoder will be updated
 * immediately upon either the A or B channel changing state.
 * 
 * Since many pins can be mapped according to the user's
 * preference the CN ISRs will all call into a common function with it's corresponding
 * button number. The common function will determine which encoder its caller is
 * mapped to and update the corresponding encoder position.
 * 
 * The algorithm used to calculate encoder position:
 *
 *			new	    new	    old	    old
 *			B   	A	    B	    A   	Result
 *			----	----	----	----	------
 *			0	0	0	0	no movement
 *			0	0	0	1	+1
 *			0	0	1	0	-1
 *			0	0	1	1	+2  (assume pin1 edges only)
 *			0	1	0	0	-1
 *			0	1	0	1	no movement
 *			0	1	1	0	-2  (assume pin1 edges only)
 *			0	1	1	1	+1
 *			1	0	0	0	+1
 *			1	0	0	1	-2  (assume pin1 edges only)
 * 			1	0	1	0	no movement
 * 			1	0	1	1	-1
 * 			1	1	0	0	+2  (assume pin1 edges only)
 *			1	1	0	1	-1
 *			1	1	1	0	+1
 *			1	1	1	1	no movement
 */

#include "system_definitions.h"

typedef struct {
    /* Last & current state of A channel */
    uint8_t A_last[NUM_ENCODERS], A_cur[NUM_ENCODERS];
    /* Last and current state of B channel */
    uint8_t B_last[NUM_ENCODERS], B_cur[NUM_ENCODERS];
    /* Position of the encoder last time GetDelta was called */
    uint8_t LastPos[NUM_ENCODERS];
    /* Position of the encoder calculated each call to Update */
    float CurPos[NUM_ENCODERS];
    
    /* The amount to increment position each tick */
    uint8_t TickIncr;
} QuadEncoderData;

/*
 * QE_Initialize()
 * 
 * Initialize the encoder driver. This must be called after
 * the IO HAL and port library have been initialized.
 */
void QE_Initialize();

/*
 * QE_GetDelta()
 * 
 * Get the position of the encoder relative to the last time this
 * function was called.
 * 
 * Parameters:
 *      encoder     Encoder # to get
 * Returns:
 *      int32       The encoder delta
 */
int32_t QE_GetDelta(uint8_t encoder);

/*
 * QE_GetPosition()
 * 
 * Gets the current position of the encoder.
 * Range: 0 -> 0xFFFFFFFF
 * 
 * Paramters:
 *      encoder     Encoder # to get
 * 
 * Returns:
 *      uint8      The current position of the encoder
 */
uint8_t QE_GetPosition(uint8_t encoder);

/*
 * QE_Update()
 * 
 * Update the position of the encoders. Must be callec every loop
 */
void QE_Update();

#endif	/* QUADENCODER_H */

