#ifndef _APP_H
#define _APP_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"
#include "usb/usb_chapter_9.h"
#include "usb/usb_device.h"

/* States that are part of the application's state machine */
typedef enum
{
    APP_STATE_INIT,
    APP_STATE_WAIT_FOR_CONFIG,
    APP_STATE_RUNNING            
} APP_STATES;

/* Data used by the application */
typedef struct
{
    //current state
    APP_STATES state;

    //USB handles
    USB_DEVICE_HANDLE  deviceHandle;
    USB_DEVICE_HID_TRANSFER_HANDLE sendTransferHandle;
    USB_DEVICE_HID_TRANSFER_HANDLE receiveTransferHandle;
    
    //unused but was included in the demo so I'm putting it here for now
    uint8_t configurationValue;

    //set when a report has been completely received
    bool isReportReady;

    //set when a report has been sent
    bool isReportSentComplete;

    //set when the PC has configured our USB device
    bool isConfigured;
    
    //unused but was included in the demo so I'm putting it here for now
    uint8_t idleRate;
    
    bool configMode;

} APP_DATA;

/* Initialize important shit */
void APP_Initialize ( void );


/* Main state machine - called every loop by the top level while loop */
void APP_Tasks ( void );


#endif /* _APP_H */
