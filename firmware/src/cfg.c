#include "cfg.h"
#include "cmds.h"
#include "eeprom.h"
#include "app.h"
#include "usbmgr.h"

#define MAGIC_ADR() 0
#define BTN_ADR(x) (2+(x-1))
#define LED_ADR(x) (20+(x-1))
#define ENC_PPR_ADR(x) (38+(x))
#define ENC_SENS_ADR(x) (42+(x))
#define RES_ERR() {CFG_SendResponse(ACK_ERR,0,0,0); break;}

uint8_t NameCurChar;

extern APP_DATA appData;
extern USB_InputReport_t* usb_input_report;

//lookup tables for quick validation
static uint8_t btn_lookup[] = {1,0,0,0,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1};
static uint8_t led_lookup[] = {0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0};

//raw data for default configuration
static uint8_t def_config[] = {
    //the magic string
    'h','b',
    //button config
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    //LED config
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    //encoder config
    0,50, 0,50,
    85,85
};

void CFG_SendResponse(uint8_t cmd, uint8_t p1, uint8_t p2, uint8_t p3)
{
    while (!appData.isReportSentComplete){}
    appData.isReportSentComplete = false;

    //report ID is always 1 for the game pad data
    usb_input_report->report_id = 1;
    usb_input_report->buttons = 0;

    //build the config command packet
    usb_input_report->cfg_cmd = cmd << 24;
    usb_input_report->cfg_cmd |= p1 << 16;
    usb_input_report->cfg_cmd |= p2 << 8;
    usb_input_report->cfg_cmd |= p3;
    //send the input report
    USB_DEVICE_HID_ReportSend(USB_DEVICE_HID_INDEX_0, &appData.sendTransferHandle, usb_input_report, sizeof(USB_InputReport_t));
}

void CFG_LoadDefaults()
{
    int j;
    //write the default values to the EEPROM
    for (j=0;j<sizeof(def_config);j++)
    {
        EEPROM_Write(j, def_config[j]);
    }
    
    //call back to CFG_Initialize now that the EEPROM is set to default
    CFG_Initialize();
}

void CFG_Initialize()
{
    /*
     * EEPROM validation uses nuke it from space strategy.
     * 
     * If invalid magic string ,
     * we just assume all of memory is corrupt and invalid
     * and load it with the default configuration
     */
    uint8_t magic1,magic2;
    uint8_t i;
    uint8_t tmp;
    
    enc_conf_list[0].A = 0;
    enc_conf_list[1].B = 0;
    
    //verify magic key
    EEPROM_Read(MAGIC_ADR(),&magic1);
    EEPROM_Read(MAGIC_ADR()+1,&magic2);
    if (magic1 != 'h' || magic2 != 'b')
    {
        CFG_LoadDefaults();
        return;
    }
    
    //read the button & LED config
    for (i=0;i<18;i++)
    {
        //button
        uint8_t enc,ab;
        EEPROM_Read(BTN_ADR(i),&tmp);
        enc = tmp >> 4;
        ab = tmp & 0x0F;
        if (enc > 2 || ab > 1) //reset to default if invalid value read
        {
            EEPROM_Write(BTN_ADR(i),def_config[BTN_ADR(i)]);
            tmp = def_config[BTN_ADR(i)];
            enc = tmp >> 4;
            ab = tmp & 0x0F;
        }
        //enable every button - ones mapped to encoders will be disabled later
        btn_config_list[i].enabled = 1;
        //set the config values for the encoder if mapped
        if (enc != 0 && ab == 0)
            enc_conf_list[enc-1].A = i+1;
        else if (enc != 0 && ab == 1)
            enc_conf_list[enc-1].B = i+1;
        //LED
        EEPROM_Read(LED_ADR(i),&tmp);
        if (tmp > 1) //reset to default if invalid value read
        {
            EEPROM_Write(LED_ADR(i),def_config[LED_ADR(i)]);
            tmp = def_config[LED_ADR(i)];
        }
        //set if the LED is dimmable
        led_config_list[i].dimmable = tmp;
    }
    
    //now we can initialize the encoders
    for (i=0;i<2;i++)
    {
        //if A and B were set, this encoder should be enabled
        //and the mapped buttons disabled
        if (enc_conf_list[i].A != 0 && enc_conf_list[i].B !=0)
        {
            enc_conf_list[i].enabled = 1;
            btn_config_list[enc_conf_list[i].A-1].enabled = 0;
            btn_config_list[enc_conf_list[i].A-1].encoder = i+1;
            btn_config_list[enc_conf_list[i].B-1].enabled = 0;
            btn_config_list[enc_conf_list[i].B-1].encoder = i+1;
            
            //read PPR high byte
            EEPROM_Read(ENC_PPR_ADR((2*i)), &tmp);
            enc_conf_list[i].ppr = ((uint16_t)tmp) << 8;
            //read PPR low byte
            EEPROM_Read(ENC_PPR_ADR((2*i)+1), &tmp);
            enc_conf_list[i].ppr |= (uint16_t) tmp;
            
            uint8_t sens;
            EEPROM_Read(ENC_SENS_ADR(i), &sens);
            enc_conf_list[i].sensitivity = (float)sens/85.0f;
        }
    }
}

int CFG_ProcessCmd(uint32_t cmd)
{
    uint8_t op = (cmd >> 24) & 0x000000FF;
    uint8_t parm1 = (cmd >> 16) & 0x000000FF;
    uint8_t parm2 = (cmd >> 8 ) & 0x000000FF;
    uint8_t parm3 = cmd & 0x000000FF;
    volatile uint8_t tmp;
    uint8_t i;
    
    //if NOP, don't do anything
    if (op == CMD_NOP)
        return 0;
    
    switch (op)
    {
        case CMD_ENTER_CFG:
            appData.configMode = true;
            break;
        case CMD_EXIT_CFG:
            appData.configMode = false;
            break;
        case CMD_QUAD:
        {
            if (parm1 != 0 && parm1 != 1 && parm1 != 2) //only encoders 1 or 2 exist!!
                RES_ERR();
            if ((parm2 < 0 || parm2 > 18 || !btn_lookup[parm2]) ||
                (parm3 < 0 || parm3 > 18 || !btn_lookup[parm3])) //check for valid buttons
            {
                RES_ERR();
            }
            
            //if buttons are both 0, disable the specified encoder and free up the buttons
            if (parm2 == 0 && parm3 == 0)
            {
                //enable buttons mapped to encoder
                for (i=0;i<18;i++)
                {
                    EEPROM_Read(BTN_ADR(i),&tmp);
                    if ((tmp >> 4) == parm1)
                        EEPROM_Write(BTN_ADR(i),0);
                }
                CFG_SendResponse(ACK_OK,0,0,0);
                break;
            }
            
            //write the config to the EEPROM for the A channel button
            //upper nibble: encoder #, lower nibble: A=0,B=1
            uint8_t v = (parm1 << 4) | 0x00;
            EEPROM_Write(BTN_ADR(parm2-1), v);
            EEPROM_Read(BTN_ADR(parm2-1),&tmp);
            if (tmp != v) //verify
                RES_ERR();
            
            //write the config to the EEPROM for the B channel button
            v = (parm1 << 4) | 0x01;
            EEPROM_Write(BTN_ADR(parm3-1), v);
            EEPROM_Read(BTN_ADR(parm3-1),&tmp);
            if (tmp != v) //verify
                RES_ERR();
            
            //send response to PC
            CFG_SendResponse(ACK_OK,0,0,0);
            break;
        }
        case CMD_QUAD_PPR:
        {
            //verify encoder validity
            if (parm1 < 1 || parm1 > 2)
                RES_ERR();
            
            //write the PPR high byte
            EEPROM_Write(ENC_PPR_ADR(2*(parm1-1)), parm2);
            EEPROM_Read(ENC_PPR_ADR(2*(parm1-1)),&tmp);
            if (tmp != parm2) //verify
                RES_ERR();
            
            //write the PPR low byte
            EEPROM_Write(ENC_PPR_ADR((2*(parm1-1))+1), parm3);
            EEPROM_Read(ENC_PPR_ADR((2*(parm1-1))+1),&tmp);
            if (tmp != parm3) //verify
                RES_ERR();
            
            //send response to PC
            CFG_SendResponse(ACK_OK,0,0,0);
            break;
        }
        case CMD_QUAD_SENS:
        {
            //verify encoder validity
            if (parm1 < 1 || parm1 > 2)
                RES_ERR();
            
            //write the sensitivity value to memory
            EEPROM_Write(ENC_SENS_ADR(parm1-1), parm2);
            EEPROM_Read(ENC_SENS_ADR(parm1-1),&tmp);
            if (tmp != parm2) //verify
                RES_ERR();
            
            //send response to PC
            CFG_SendResponse(ACK_OK,0,0,0);
            
            break;
        }
        case CMD_SET_LED:
        {
            if (parm1 < 0 || parm1 > 18 || !led_lookup[parm1]) //check for valid LED
                RES_ERR();
            
            //write the config to the EEPROM
            //byte @ address: 1 if dimmable, 0 if not
            EEPROM_Write(LED_ADR(parm1-1), parm2);
            EEPROM_Read(LED_ADR(parm1-1),&tmp);
            if (tmp != parm2) //verify
                RES_ERR();
            
            //send response to PC
            CFG_SendResponse(ACK_OK,0,0,0);
            break;
        }
        case CMD_REQ_BTN:
        {
            if (parm1 < 1 || parm1 > 18)
                RES_ERR();
            
            //read the assignment
            uint8_t enc,ab;
            EEPROM_Read(BTN_ADR(parm1-1), &tmp);
            enc = tmp >> 4;
            ab = tmp & 0x0F;
            
            //send the response
            CFG_SendResponse(ACK_BTN,enc,ab,0);
            break;
        }
        case CMD_REQ_LED:
        {
            if (parm1 < 1 || parm1 > 18)
                RES_ERR();
            
            //read the assignment
            EEPROM_Read(LED_ADR(parm1-1), &tmp);
            //send the response
            CFG_SendResponse(ACK_LED,tmp,0,0);
            break;
        }
        case CMD_REQ_QUAD:
        {
            if (parm1 < 1 || parm1 > 2)
                RES_ERR();
            
           uint8_t pprhigh, pprlow, sens;
            
            //read the PPR high byte
            EEPROM_Read(ENC_PPR_ADR((2*(parm1-1))), &tmp);
            pprhigh = tmp;
            //read the PPR low byte
            EEPROM_Read(ENC_PPR_ADR((2*(parm1-1))+1), &tmp);
            
            //read the sensitivity
            EEPROM_Read(ENC_SENS_ADR(parm1-1), &sens);
            
            //send the response
            CFG_SendResponse(ACK_QUAD, pprhigh, tmp, sens);
            
            break;
        }
    }
    
    return 1;
}
