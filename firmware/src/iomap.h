#ifndef IOMAP_H
#define	IOMAP_H

#include "system_definitions.h"

/* These defines map buttons & LEDs to physical pins */

//LEDs
#define IO_LED_1 LATEbits.LATE5
#define IO_LED_2 LATEbits.LATE6
#define IO_LED_3 LATEbits.LATE7
#define IO_LED_4 LATGbits.LATG6
#define IO_LED_5 LATGbits.LATG7
#define IO_LED_6 LATGbits.LATG8
#define IO_LED_7 LATGbits.LATG9
#define IO_LED_8 LATBbits.LATB5
#define IO_LED_9 LATBbits.LATB4
#define IO_LED_10 LATBbits.LATB3
#define IO_LED_11 LATBbits.LATB2
#define IO_LED_12 LATBbits.LATB6
#define IO_LED_13 LATBbits.LATB7
#define IO_LED_14 LATBbits.LATB8
#define IO_LED_15 LATBbits.LATB9
#define IO_LED_16 LATBbits.LATB10
#define IO_LED_17 LATBbits.LATB11
#define IO_LED_18 LATBbits.LATB12
        
//BUTTONS
#define IO_BTN_1 PORTBbits.RB13
#define IO_BTN_2 PORTBbits.RB14
#define IO_BTN_3 PORTBbits.RB15
#define IO_BTN_4 PORTFbits.RF4
#define IO_BTN_5 PORTFbits.RF5
#define IO_BTN_6 PORTDbits.RD9
#define IO_BTN_7 PORTDbits.RD10
#define IO_BTN_8 PORTDbits.RD11
#define IO_BTN_9 PORTDbits.RD0
#define IO_BTN_10 PORTCbits.RC13
#define IO_BTN_11 PORTCbits.RC14
#define IO_BTN_12 PORTDbits.RD5
#define IO_BTN_13 PORTFbits.RF0
#define IO_BTN_14 PORTFbits.RF1
#define IO_BTN_15 PORTEbits.RE0
#define IO_BTN_16 PORTEbits.RE1
#define IO_BTN_17 PORTEbits.RE2
#define IO_BTN_18 PORTEbits.RE3

#endif	/* IOMAP_H */
