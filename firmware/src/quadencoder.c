#include <math.h>
#include "quadencoder.h"
#include "cfg.h"
#include "iohal.h"

//the driver data structure
QuadEncoderData qeData;

void QE_Initialize()
{
    uint8_t i;
    EncoderConfig encCfg;
        
    //read the current state of A/B for all encoders to prevent jumps
    for (i=0;i<NUM_ENCODERS;i++)
    {
        encCfg = enc_conf_list[i];
        
        //is this encoder enabled?
        if (!encCfg.enabled)
            continue;
        
        //set the A values
        qeData.A_cur[i] = IO_BTN_GetSingle(encCfg.A);
        qeData.A_last[i] = qeData.A_cur[i];
        
        //set the B values
        qeData.B_cur[i] = IO_BTN_GetSingle(encCfg.B);
        qeData.B_last[i] = qeData.B_cur[i];
        
        //reset the position
        qeData.LastPos[0] = 0;
        qeData.CurPos[0] = 0;
        qeData.LastPos[1] = 0;
        qeData.CurPos[1] = 0;
    }
}

int32_t QE_GetDelta(uint8_t encoder)
{
    int32_t delta;
    
    //return 0 if the encoder is disabled or the value passed is invalid
    if (encoder > 1 || enc_conf_list[encoder].enabled == 0)
        return 0;
    
    //otherwise calculate the delta
    delta = qeData.CurPos[encoder] - qeData.LastPos[encoder];
    
    //update the last position
    qeData.LastPos[encoder] = qeData.CurPos[encoder];
    
    return delta;
}

uint8_t QE_GetPosition(uint8_t encoder)
{
    //return 0 if encoder is disabled or the value passed is invalid
    if (encoder > 1 || enc_conf_list[encoder].enabled == 0)
        return 0;
    
    return (uint8_t)abs(qeData.CurPos[encoder]);
}

void QE_Update()
{
    uint8_t i=0;

    //for each encoder, check the buttons and figure out the change in position
    for (i=0;i<2;i++)
    {
        if (enc_conf_list[i].enabled == 0)
            continue;
        
        qeData.A_cur[i] = IO_BTN_GetSingle(enc_conf_list[i].A);
        qeData.B_cur[i] = IO_BTN_GetSingle(enc_conf_list[i].B);
        
        //calculate the increment for this encoder
        float incr = (1.0f/(float)enc_conf_list[i].ppr) * 255.0f;
        incr *= enc_conf_list[i].sensitivity;
        
        
        uint8_t a = (qeData.B_cur[i] << 3) | (qeData.A_cur[i] << 2) |
                    (qeData.B_last[i] << 1) | (qeData.A_last[i]);
        switch (a)
        {
            case 0:
            case 0b00000101:
            case 0b00001010:
            case 0b00001111:
            default:
                //no movement
                break;
            case 0b0001:
            case 0b0111:
            case 0b1000:
            case 0b1110:
                qeData.CurPos[i] += incr;
                break;
            case 0b0010:
            case 0b0100:
            case 0b1011:
            case 0b1101:
                qeData.CurPos[i] -= incr;
                break;
            case 0b0011:
            case 0b1100:
                qeData.CurPos[i] += incr*2;
                break;
            case 0b0110:
            case 0b1001:
                qeData.CurPos[i] -= incr*2;
                break;
        }
        
        qeData.A_last[i] = qeData.A_cur[i];
        qeData.B_last[i] = qeData.B_cur[i];
        
        if (qeData.CurPos[i] > 255)
            qeData.CurPos[i] = 0;
        else if (qeData.CurPos[i] < 0)
            qeData.CurPos[i] = 255;
    }
}