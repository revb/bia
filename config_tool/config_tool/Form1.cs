﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HidLibrary;

namespace config_tool
{
    public partial class Form1 : Form
    {
        public const int VID = 0x0420;
        public const int PID = 0x6911;

        HidDevice dev_bia;
        CommProtocol comprt;

        List<CheckBox> ledCheckBoxes; //lookup table for LED checkboxes
        List<int> validEncBtns;
        int[] validDimLeds;

        public Form1()
        {
            InitializeComponent();
            comprt = null;
        }

        private void RebuildButtonLists(object sender, EventArgs e)
        {
            //back up the selected item because clear kills it
            string s1a,s1b,s2a,s2b;
            s1a = (string)cmb_enc1_a.SelectedItem;
            s1b = (string)cmb_enc1_b.SelectedItem;
            s2a = (string)cmb_enc2_a.SelectedItem;
            s2b = (string)cmb_enc2_b.SelectedItem;

            //if either A or B is set to not mapped, set the other as well
            //note: this only checks based on the combo box change that trigerred
            //this change
            if (sender != null && (string)((ComboBox)sender).SelectedItem == "<Not Mapped>")
            {
                if (sender == cmb_enc1_a || sender == cmb_enc1_b)
                {
                    s1a = "<Not Mapped>";
                    s1b = "<Not Mapped>";
                }
                if (sender == cmb_enc2_a || sender == cmb_enc2_b)
                {
                    s2a = "<Not Mapped>";
                    s2b = "<Not Mapped>";
                }
            }

            //clear the lists for fresh start
            cmb_enc1_a.Items.Clear();
            cmb_enc1_b.Items.Clear();
            cmb_enc2_a.Items.Clear();
            cmb_enc2_b.Items.Clear();

            //add all buttons to all lists as long as they aren't already selected
            cmb_enc1_a.Items.Add("<Not Mapped>");
            cmb_enc1_b.Items.Add("<Not Mapped>");
            cmb_enc2_a.Items.Add("<Not Mapped>");
            cmb_enc2_b.Items.Add("<Not Mapped>");
            for (int i = 0; i < validEncBtns.Count; i++)
            {
                string curbtn = "Button " + validEncBtns[i].ToString();
                if (s1a == curbtn)
                    cmb_enc1_a.Items.Add(curbtn);
                else if (s1b == curbtn)
                    cmb_enc1_b.Items.Add(curbtn);
                else if (s2a == curbtn)
                    cmb_enc2_a.Items.Add(curbtn);
                else if (s2b == curbtn)
                    cmb_enc2_b.Items.Add(curbtn);
                else
                {
                    cmb_enc1_a.Items.Add(curbtn);
                    cmb_enc1_b.Items.Add(curbtn);
                    cmb_enc2_a.Items.Add(curbtn);
                    cmb_enc2_b.Items.Add(curbtn);
                }
            }

            //restore the selected item
            cmb_enc1_a.SelectedItem = s1a;
            cmb_enc1_b.SelectedItem = s1b;
            cmb_enc2_a.SelectedItem = s2a;
            cmb_enc2_b.SelectedItem = s2b;
        }

        private void LoadConfig(bool connected)
        {
            //if not connected disable the config window and do nothing
            if (!connected)
            {
                groupBox2.Enabled = false;
                btn_reset.Enabled = false;
                btn_save.Enabled = false;
                return;
            }

            groupBox2.Enabled = true;
            btn_reset.Enabled = true;
            btn_save.Enabled = true;

            //build the combo boxes for encoders
            EncoderConfig ec1 = comprt.GetEncoderConfig(1);
            EncoderConfig ec2 = comprt.GetEncoderConfig(2);
            RebuildButtonLists(null, null);
            //encoder 1
            nbr_enc1_ppr.Value = ec1.ppr;
            nbr_enc1_sens.Value = (decimal)ec1.sensitivity;
            chk_enc1_enabled.Checked = ec1.enabled;
            if (ec1.enabled)
            {
                cmb_enc1_a.SelectedItem = "Button " + ec1.A.ToString();
                cmb_enc1_b.SelectedItem = "Button " + ec1.B.ToString();
            }
            else
            {
                cmb_enc1_a.SelectedItem = "<Not Mapped>";
                cmb_enc1_b.SelectedItem = "<Not Mapped>";
            }
            //encoder 2
            nbr_enc2_ppr.Value = ec2.ppr;
            nbr_enc2_sens.Value = (decimal)ec2.sensitivity;
            chk_enc2_enabled.Checked = ec2.enabled;
            if (ec2.enabled)
            {
                cmb_enc2_a.SelectedItem = "Button " + ec2.A.ToString();
                cmb_enc2_b.SelectedItem = "Button " + ec2.B.ToString();
            }
            else
            {
                cmb_enc2_a.SelectedItem = "<Not Mapped>";
                cmb_enc2_b.SelectedItem = "<Not Mapped>";
            }
            //rebuild again now that stuff has been selected
            RebuildButtonLists(null, null);

            //build the LED checkboxes
            for (int i=0;i<18;i++)
            {
                if (comprt.GetLEDDimmable(i+1))
                    ledCheckBoxes[i].Checked = true;
                else
                    ledCheckBoxes[i].Checked = false;
            }
        }

        private void SetIsConnected(bool connected, string name)
        {
            if (connected)
            {
                lbl_status.BackColor = Color.Green;
                lbl_status.Text = "Connected to: " + name;
                grp_config.Enabled = true;

                comprt = new CommProtocol(ref dev_bia);
                comprt.EnterConfigMode();
            }
            else
            {
                lbl_status.BackColor = Color.Red;
                lbl_status.Text = "Not Connected";
                grp_config.Enabled = false;
            }

            LoadConfig(connected);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //initialize the lookup tables
            ledCheckBoxes = new List<CheckBox>();
            ledCheckBoxes.Add(checkBox1); ledCheckBoxes.Add(checkBox2); ledCheckBoxes.Add(checkBox3);
            ledCheckBoxes.Add(checkBox4); ledCheckBoxes.Add(checkBox5); ledCheckBoxes.Add(checkBox6);
            ledCheckBoxes.Add(checkBox7); ledCheckBoxes.Add(checkBox8); ledCheckBoxes.Add(checkBox9);
            ledCheckBoxes.Add(checkBox10); ledCheckBoxes.Add(checkBox11); ledCheckBoxes.Add(checkBox12);
            ledCheckBoxes.Add(checkBox13); ledCheckBoxes.Add(checkBox14); ledCheckBoxes.Add(checkBox15);
            ledCheckBoxes.Add(checkBox16); ledCheckBoxes.Add(checkBox17); ledCheckBoxes.Add(checkBox18);

            validEncBtns = new List<int>();
            validEncBtns.Add(4); validEncBtns.Add(5); validEncBtns.Add(6); validEncBtns.Add(7); validEncBtns.Add(8);
            validEncBtns.Add(9); validEncBtns.Add(12); validEncBtns.Add(13); validEncBtns.Add(14); validEncBtns.Add(15);
            validEncBtns.Add(16); validEncBtns.Add(17); validEncBtns.Add(18);

            validDimLeds = new int[] { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            HidDevice[] hidDevList = HidDevices.Enumerate(VID, PID).ToArray<HidDevice>();
            if (hidDevList.Length == 0)
            {
                //VID/PID not found
                SetIsConnected(false, "");
                return;
            }
            //get the device
            dev_bia = hidDevList[0];
            byte[] product;
            dev_bia.ReadProduct(out product);
            SetIsConnected(true, System.Text.Encoding.Unicode.GetString(product,0,product.Length));
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (comprt != null)
                comprt.ExitConfigMode();
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            //reset the encoder stuff
            //combo boxes
            RebuildButtonLists(null, null);
            cmb_enc1_a.SelectedItem = "<Not Mapped>";
            cmb_enc1_b.SelectedItem = "<Not Mapped>";
            cmb_enc2_a.SelectedItem = "<Not Mapped>";
            cmb_enc2_b.SelectedItem = "<Not Mapped>";
            RebuildButtonLists(null, null);
            //checkboxes
            chk_enc1_enabled.Checked = false;
            chk_enc2_enabled.Checked = false;
            //ppr and sensitivity
            nbr_enc1_ppr.Value = 50;
            nbr_enc1_sens.Value = 1;
            nbr_enc2_ppr.Value = 50;
            nbr_enc2_sens.Value = 1;

            //reset the LED stuff
            for (int i=0;i<ledCheckBoxes.Count;i++)
            {
                ledCheckBoxes[i].Checked = false;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            //we need to write every configuration setting to the board
            //any of these can fail, so we will run everything and then check
            //to see if anything failed

            bool failed = false;

            //write the encoder data
            string s1a = (string)cmb_enc1_a.SelectedItem;
            string s1b = (string)cmb_enc1_b.SelectedItem;
            string s2a = (string)cmb_enc2_a.SelectedItem;
            string s2b = (string)cmb_enc2_b.SelectedItem;
            //encoder 1
            failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD_PPR, 1, (byte)((int)nbr_enc1_ppr.Value >> 8), (byte)((int)nbr_enc1_ppr.Value & 0xFF), true);
            failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD_SENS, 1, (byte)(nbr_enc1_sens.Value*85), 0, true);
            if (chk_enc1_enabled.Checked && s1a != "<Not Mapped>" && s1b != "<Not Mapped>")
                failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD, 1, (byte)int.Parse(s1a[s1a.Length - 1].ToString()), (byte)int.Parse(s1b[s1b.Length - 1].ToString()), true);
            else
                failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD, 1, 0, 0, true);
            //encoder 2
            failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD_PPR, 2, (byte)((int)nbr_enc2_ppr.Value >> 8), (byte)((int)nbr_enc2_ppr.Value & 0xFF), true);
            failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD_SENS, 2, (byte)(nbr_enc2_sens.Value*85), 0, true);
            if (chk_enc2_enabled.Checked && s2a != "<Not Mapped>" && s2b != "<Not Mapped>")
                failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD, 2, (byte)int.Parse(s2a[s2a.Length - 1].ToString()), (byte)int.Parse(s2b[s2b.Length - 1].ToString()), true);
            else
                failed |= !comprt.SendMessage(CommProtocol.CMD_QUAD, 2, 0, 0, true);

            //write the LED data
            for (int i=0;i<ledCheckBoxes.Count;i++)
            {
                if (validDimLeds[i + 1] == 0)
                    continue;
                failed |= !comprt.SendMessage(CommProtocol.CMD_SET_LED, (byte)(i+1), (byte)(ledCheckBoxes[i].Checked ? 1 : 0), 0, true);
            }

            if (failed)
            {
                MessageBox.Show("Error", "Something failed! The config tool should be smart enough to prevent you " +
                                         "from setting something invalid. Restart the program, power cycle your BIA, " +
                                         "and try again. Same problem? Contact me via PM with screenshots.");
            }
            else
            {
                MessageBox.Show("Success!", "Configuration successfully written to board");
            }
        }
    }
}
