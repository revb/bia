﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using HidLibrary;

namespace config_tool
{
    struct OutputReport
    {
        public byte ReportID;
        public byte LED1, LED2, LED3;
        public byte CMDParm3, CMDParm2, CMDParm1, CMDOp;

    };
    struct EncoderConfig
    {
        public bool enabled;
        public int A, B;
        public int ppr;
        public float sensitivity;

        public static EncoderConfig Ded()
        {
            EncoderConfig r;
            r.enabled = false;
            r.A = 0;
            r.B = 0;
            r.ppr = 0;
            r.sensitivity = 0;
            return r;
        }
    };

    class CommProtocol
    {
        public const byte CMD_QUAD = 0x10;
        public const byte CMD_QUAD_PPR = 0x11;
        public const byte CMD_QUAD_SENS = 0x12;
        public const byte CMD_SET_LED = 0x20;
        public const byte CMD_NAME_START = 0x30;
        public const byte CMD_NAME_STR = 0x31;
        public const byte CMD_REQ_BTN = 0x40;
        public const byte CMD_REQ_LED = 0x41;
        public const byte CMD_REQ_QUAD = 0x42;

        public const byte ACK_OK = 0x10;
        public const byte ACK_ERR = 0x11;
        public const byte ACK_BTN = 0x20;
        public const byte ACK_LED = 0x30;
        public const byte ACK_NAME = 0x40;
        public const byte ACK_QUAD = 0x50;

        const int NAME_MAX_LENGTH = 20;

        HidDevice m_hidDev;

        public CommProtocol(ref HidDevice dev)
        {
            m_hidDev = dev;
        }

        public bool SendMessage(byte opcode, byte p1, byte p2, byte p3, bool response)
        {
            OutputReport oreport = new OutputReport();
            byte[] OutData = new byte[8];

            //create the report
            oreport.ReportID = 2;
            oreport.CMDOp = opcode;
            oreport.CMDParm1 = p1;
            oreport.CMDParm2 = p2;
            oreport.CMDParm3 = p3;

            //get a byte array from the report structure
            IntPtr ptr = Marshal.AllocHGlobal(8);
            Marshal.StructureToPtr(oreport, ptr, true);
            Marshal.Copy(ptr, OutData, 0, 8);
            Marshal.FreeHGlobal(ptr);

            //write the report
            m_hidDev.Write(OutData);

            //get the response from the board
            if (response)
            {
                HidDeviceData InData = m_hidDev.Read();
                while (InData.Data[9] != ACK_ERR && InData.Data[9] != ACK_OK) InData = m_hidDev.Read();
                return (InData.Data[9] == ACK_OK);
            }
            return true;
        }
        public void EnterConfigMode()
        {
            OutputReport oreport = new OutputReport();
            byte[] OutData = new byte[8];

            //create the report
            oreport.ReportID = 2;
            oreport.CMDOp = 0x01;

            //get a byte array from the report structure
            IntPtr ptr = Marshal.AllocHGlobal(8);
            Marshal.StructureToPtr(oreport, ptr, true);
            Marshal.Copy(ptr, OutData, 0, 8);
            Marshal.FreeHGlobal(ptr);
            //write the data to USB
            m_hidDev.Write(OutData);
        }
        public void ExitConfigMode()
        {
            SendMessage(0x02, 0, 0, 0, false);
        }

        public EncoderConfig GetEncoderConfig(int encoder)
        {
            EncoderConfig ret;
            OutputReport oreport = new OutputReport();
            byte[] OutData = new byte[8];

            //initialize the encoder config structure
            ret.enabled = false;
            ret.A = 0;
            ret.B = 0;
            ret.ppr = 0;
            ret.sensitivity = 0;

            //get the ppr and sensitivity
            oreport.ReportID = 2;
            oreport.CMDOp = CMD_REQ_QUAD;
            oreport.CMDParm1 = (byte)encoder;
            //convert report to byte array
            IntPtr ptr = Marshal.AllocHGlobal(8);
            Marshal.StructureToPtr(oreport, ptr, true);
            Marshal.Copy(ptr, OutData, 0, 8);
            Marshal.FreeHGlobal(ptr);
            //write the report to USB
            m_hidDev.Write(OutData);
            //get the response
            HidDeviceData InData = m_hidDev.Read();
            while (InData.Data[9] != ACK_QUAD && InData.Data[9] != ACK_ERR) InData = m_hidDev.Read();
            ret.ppr = (InData.Data[8] << 8) | InData.Data[7];
            ret.sensitivity = ((float)InData.Data[6]) / 85.0f;


            //initialize an output report to get button info
            oreport.ReportID = 2;
            oreport.CMDOp = CMD_REQ_BTN;

            //request the status of every button, ignoring everything but
            //the ones related to the requested encoder
            for (int i = 0; i < 18; i++)
            {
                oreport.CMDParm1 = (byte)(i+1);

                //convert report to byte array
                ptr = Marshal.AllocHGlobal(8);
                Marshal.StructureToPtr(oreport, ptr, true);
                Marshal.Copy(ptr, OutData, 0, 8);
                Marshal.FreeHGlobal(ptr);
                //write the report to USB
                m_hidDev.Write(OutData);
                //get the response
                InData = m_hidDev.Read();
                while (InData.Data[9] != ACK_BTN) InData = m_hidDev.Read();

                //check if set to the encoder we're looking for
                if (InData.Data[8] == encoder && InData.Data[7] == 0)
                {
                    //this is the A input
                    ret.A = i+1;
                }
                else if (InData.Data[8] == encoder && InData.Data[7] == 1)
                {
                    //this is the B input
                    ret.B = i+1;
                }
            }

            //if A and B are set, this encoder is in use
            if (ret.A != 0 && ret.B != 0)
                ret.enabled = true;

            return ret;
        }
        public bool GetLEDDimmable(int led)
        {
            OutputReport oreport = new OutputReport();
            byte[] OutData = new byte[8];

            //create the report
            oreport.ReportID = 2;
            oreport.CMDOp = CMD_REQ_LED;
            oreport.CMDParm1 = (byte)led;

            //get a byte array from the report structure
            IntPtr ptr = Marshal.AllocHGlobal(8);
            Marshal.StructureToPtr(oreport, ptr, true);
            Marshal.Copy(ptr, OutData, 0, 8);
            Marshal.FreeHGlobal(ptr);

            //write the report
            m_hidDev.Write(OutData);

            //get the response from the board
            HidDeviceData InData = m_hidDev.Read();
            while (InData.Data[9] != ACK_LED) InData = m_hidDev.Read();

            return (InData.Data[8] == led);
        }
    }
}
