﻿namespace config_tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grp_hid = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_status = new System.Windows.Forms.Label();
            this.grp_config = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nbr_enc2_sens = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nbr_enc2_ppr = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.chk_enc2_enabled = new System.Windows.Forms.CheckBox();
            this.cmb_enc2_b = new System.Windows.Forms.ComboBox();
            this.cmb_enc2_a = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nbr_enc1_sens = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nbr_enc1_ppr = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.chk_enc1_enabled = new System.Windows.Forms.CheckBox();
            this.cmb_enc1_b = new System.Windows.Forms.ComboBox();
            this.cmb_enc1_a = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.grp_hid.SuspendLayout();
            this.grp_config.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc2_sens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc2_ppr)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc1_sens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc1_ppr)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_hid
            // 
            this.grp_hid.Controls.Add(this.button1);
            this.grp_hid.Controls.Add(this.lbl_status);
            this.grp_hid.Dock = System.Windows.Forms.DockStyle.Top;
            this.grp_hid.Location = new System.Drawing.Point(0, 0);
            this.grp_hid.Name = "grp_hid";
            this.grp_hid.Size = new System.Drawing.Size(747, 77);
            this.grp_hid.TabIndex = 0;
            this.grp_hid.TabStop = false;
            this.grp_hid.Text = "BIA Status";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lbl_status
            // 
            this.lbl_status.BackColor = System.Drawing.Color.Red;
            this.lbl_status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_status.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_status.Location = new System.Drawing.Point(3, 18);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(741, 27);
            this.lbl_status.TabIndex = 0;
            this.lbl_status.Text = "Not Connected";
            this.lbl_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grp_config
            // 
            this.grp_config.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grp_config.Controls.Add(this.tabControl1);
            this.grp_config.Location = new System.Drawing.Point(0, 77);
            this.grp_config.Name = "grp_config";
            this.grp_config.Size = new System.Drawing.Size(735, 307);
            this.grp_config.TabIndex = 1;
            this.grp_config.TabStop = false;
            this.grp_config.Text = "BIA Configuration";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 18);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(729, 283);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(721, 254);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Encoders";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nbr_enc2_sens);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.nbr_enc2_ppr);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.chk_enc2_enabled);
            this.groupBox2.Controls.Add(this.cmb_enc2_b);
            this.groupBox2.Controls.Add(this.cmb_enc2_a);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(355, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(363, 248);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Encoder 2";
            // 
            // nbr_enc2_sens
            // 
            this.nbr_enc2_sens.DecimalPlaces = 2;
            this.nbr_enc2_sens.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nbr_enc2_sens.Location = new System.Drawing.Point(94, 135);
            this.nbr_enc2_sens.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nbr_enc2_sens.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            131072});
            this.nbr_enc2_sens.Name = "nbr_enc2_sens";
            this.nbr_enc2_sens.Size = new System.Drawing.Size(120, 22);
            this.nbr_enc2_sens.TabIndex = 12;
            this.nbr_enc2_sens.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Sensitivity: ";
            // 
            // nbr_enc2_ppr
            // 
            this.nbr_enc2_ppr.Location = new System.Drawing.Point(94, 107);
            this.nbr_enc2_ppr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nbr_enc2_ppr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbr_enc2_ppr.Name = "nbr_enc2_ppr";
            this.nbr_enc2_ppr.Size = new System.Drawing.Size(120, 22);
            this.nbr_enc2_ppr.TabIndex = 10;
            this.nbr_enc2_ppr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "PPR:";
            // 
            // chk_enc2_enabled
            // 
            this.chk_enc2_enabled.AutoSize = true;
            this.chk_enc2_enabled.Location = new System.Drawing.Point(94, 183);
            this.chk_enc2_enabled.Name = "chk_enc2_enabled";
            this.chk_enc2_enabled.Size = new System.Drawing.Size(82, 21);
            this.chk_enc2_enabled.TabIndex = 8;
            this.chk_enc2_enabled.Text = "Enabled";
            this.chk_enc2_enabled.UseVisualStyleBackColor = true;
            // 
            // cmb_enc2_b
            // 
            this.cmb_enc2_b.FormattingEnabled = true;
            this.cmb_enc2_b.Location = new System.Drawing.Point(94, 65);
            this.cmb_enc2_b.Name = "cmb_enc2_b";
            this.cmb_enc2_b.Size = new System.Drawing.Size(171, 24);
            this.cmb_enc2_b.TabIndex = 7;
            this.cmb_enc2_b.SelectionChangeCommitted += new System.EventHandler(this.RebuildButtonLists);
            // 
            // cmb_enc2_a
            // 
            this.cmb_enc2_a.FormattingEnabled = true;
            this.cmb_enc2_a.Location = new System.Drawing.Point(94, 22);
            this.cmb_enc2_a.Name = "cmb_enc2_a";
            this.cmb_enc2_a.Size = new System.Drawing.Size(171, 24);
            this.cmb_enc2_a.TabIndex = 6;
            this.cmb_enc2_a.SelectionChangeCommitted += new System.EventHandler(this.RebuildButtonLists);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "B Channel:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "A Channel: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nbr_enc1_sens);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nbr_enc1_ppr);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.chk_enc1_enabled);
            this.groupBox1.Controls.Add(this.cmb_enc1_b);
            this.groupBox1.Controls.Add(this.cmb_enc1_a);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 248);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encoder 1";
            // 
            // nbr_enc1_sens
            // 
            this.nbr_enc1_sens.DecimalPlaces = 2;
            this.nbr_enc1_sens.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nbr_enc1_sens.Location = new System.Drawing.Point(94, 138);
            this.nbr_enc1_sens.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nbr_enc1_sens.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            131072});
            this.nbr_enc1_sens.Name = "nbr_enc1_sens";
            this.nbr_enc1_sens.Size = new System.Drawing.Size(120, 22);
            this.nbr_enc1_sens.TabIndex = 8;
            this.nbr_enc1_sens.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Sensitivity: ";
            // 
            // nbr_enc1_ppr
            // 
            this.nbr_enc1_ppr.Location = new System.Drawing.Point(94, 110);
            this.nbr_enc1_ppr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nbr_enc1_ppr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbr_enc1_ppr.Name = "nbr_enc1_ppr";
            this.nbr_enc1_ppr.Size = new System.Drawing.Size(120, 22);
            this.nbr_enc1_ppr.TabIndex = 6;
            this.nbr_enc1_ppr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "PPR:";
            // 
            // chk_enc1_enabled
            // 
            this.chk_enc1_enabled.AutoSize = true;
            this.chk_enc1_enabled.Location = new System.Drawing.Point(94, 183);
            this.chk_enc1_enabled.Name = "chk_enc1_enabled";
            this.chk_enc1_enabled.Size = new System.Drawing.Size(82, 21);
            this.chk_enc1_enabled.TabIndex = 4;
            this.chk_enc1_enabled.Text = "Enabled";
            this.chk_enc1_enabled.UseVisualStyleBackColor = true;
            // 
            // cmb_enc1_b
            // 
            this.cmb_enc1_b.FormattingEnabled = true;
            this.cmb_enc1_b.Location = new System.Drawing.Point(94, 65);
            this.cmb_enc1_b.Name = "cmb_enc1_b";
            this.cmb_enc1_b.Size = new System.Drawing.Size(171, 24);
            this.cmb_enc1_b.TabIndex = 3;
            this.cmb_enc1_b.SelectionChangeCommitted += new System.EventHandler(this.RebuildButtonLists);
            // 
            // cmb_enc1_a
            // 
            this.cmb_enc1_a.FormattingEnabled = true;
            this.cmb_enc1_a.Location = new System.Drawing.Point(94, 22);
            this.cmb_enc1_a.Name = "cmb_enc1_a";
            this.cmb_enc1_a.Size = new System.Drawing.Size(171, 24);
            this.cmb_enc1_a.TabIndex = 2;
            this.cmb_enc1_a.SelectionChangeCommitted += new System.EventHandler(this.RebuildButtonLists);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "B Channel:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "A Channel: ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBox18);
            this.tabPage2.Controls.Add(this.checkBox17);
            this.tabPage2.Controls.Add(this.checkBox16);
            this.tabPage2.Controls.Add(this.checkBox15);
            this.tabPage2.Controls.Add(this.checkBox14);
            this.tabPage2.Controls.Add(this.checkBox13);
            this.tabPage2.Controls.Add(this.checkBox12);
            this.tabPage2.Controls.Add(this.checkBox11);
            this.tabPage2.Controls.Add(this.checkBox10);
            this.tabPage2.Controls.Add(this.checkBox9);
            this.tabPage2.Controls.Add(this.checkBox8);
            this.tabPage2.Controls.Add(this.checkBox7);
            this.tabPage2.Controls.Add(this.checkBox6);
            this.tabPage2.Controls.Add(this.checkBox5);
            this.tabPage2.Controls.Add(this.checkBox4);
            this.tabPage2.Controls.Add(this.checkBox3);
            this.tabPage2.Controls.Add(this.checkBox2);
            this.tabPage2.Controls.Add(this.checkBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(721, 254);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "LEDs";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Enabled = false;
            this.checkBox18.Location = new System.Drawing.Point(151, 114);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(147, 21);
            this.checkBox18.TabIndex = 17;
            this.checkBox18.Text = "Dimmable: LED 18";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Enabled = false;
            this.checkBox17.Location = new System.Drawing.Point(6, 114);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(147, 21);
            this.checkBox17.TabIndex = 16;
            this.checkBox17.Text = "Dimmable: LED 17";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Enabled = false;
            this.checkBox16.Location = new System.Drawing.Point(441, 87);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(147, 21);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "Dimmable: LED 16";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Enabled = false;
            this.checkBox15.Location = new System.Drawing.Point(296, 87);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(147, 21);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "Dimmable: LED 15";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Enabled = false;
            this.checkBox14.Location = new System.Drawing.Point(151, 87);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(147, 21);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "Dimmable: LED 14";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Enabled = false;
            this.checkBox13.Location = new System.Drawing.Point(6, 87);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(147, 21);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "Dimmable: LED 13";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Enabled = false;
            this.checkBox12.Location = new System.Drawing.Point(441, 60);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(147, 21);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "Dimmable: LED 12";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Enabled = false;
            this.checkBox11.Location = new System.Drawing.Point(296, 60);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(147, 21);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "Dimmable: LED 11";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Enabled = false;
            this.checkBox10.Location = new System.Drawing.Point(151, 60);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(147, 21);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "Dimmable: LED 10";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Enabled = false;
            this.checkBox9.Location = new System.Drawing.Point(6, 60);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(139, 21);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "Dimmable: LED 9";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Enabled = false;
            this.checkBox8.Location = new System.Drawing.Point(441, 33);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(139, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "Dimmable: LED 8";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(296, 33);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(139, 21);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "Dimmable: LED 7";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(151, 33);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(139, 21);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "Dimmable: LED 6";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(6, 33);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(139, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "Dimmable: LED 5";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(441, 6);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(139, 21);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "Dimmable: LED 4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Enabled = false;
            this.checkBox3.Location = new System.Drawing.Point(296, 6);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(139, 21);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "Dimmable: LED 3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(151, 6);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(139, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Dimmable: LED 2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(6, 6);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(139, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Dimmable: LED 1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(721, 254);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "General";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_save.Location = new System.Drawing.Point(0, 429);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(747, 39);
            this.btn_save.TabIndex = 2;
            this.btn_save.Text = "Save Configuration";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_reset.Location = new System.Drawing.Point(0, 390);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(747, 39);
            this.btn_reset.TabIndex = 3;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 468);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.grp_config);
            this.Controls.Add(this.grp_hid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "BIA Config Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grp_hid.ResumeLayout(false);
            this.grp_config.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc2_sens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc2_ppr)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc1_sens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbr_enc1_ppr)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grp_hid;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.GroupBox grp_config;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmb_enc2_b;
        private System.Windows.Forms.ComboBox cmb_enc2_a;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmb_enc1_b;
        private System.Windows.Forms.ComboBox cmb_enc1_a;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.CheckBox chk_enc2_enabled;
        private System.Windows.Forms.CheckBox chk_enc1_enabled;
        private System.Windows.Forms.NumericUpDown nbr_enc2_sens;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nbr_enc2_ppr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nbr_enc1_sens;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nbr_enc1_ppr;
        private System.Windows.Forms.Label label5;
    }
}

